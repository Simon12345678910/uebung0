package at.htl;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Aufgabe0 extends JFrame {

    private JButton btnP;
    private JButton btnM;
    private JButton btnMult;
    private JTextField txtO;
    private JTextField txtI;
    private JTextField txtI2;
    private JPanel Panel;

    public Aufgabe0(String title){

        super(title);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(Panel);
        this.pack();


        btnP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String a = txtI.getText();
                int i = Integer.parseInt(a);

                String b = txtI2.getText();
                int i2 = Integer.parseInt(b);

                txtO.setText(""+(i+i2));


            }
        });
        btnM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                String a = txtI.getText();
                int i = Integer.parseInt(a);

                String b = txtI2.getText();
                int i2 = Integer.parseInt(b);

                txtO.setText(""+(i-i2));

            }
        });
        btnMult.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String a = txtI.getText();
                int i = Integer.parseInt(a);

                String b = txtI2.getText();
                int i2 = Integer.parseInt(b);

                txtO.setText(""+(i*i2));

            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new Aufgabe0("GUI");
        frame.setVisible(true);
    }

}
